package com.example.helloworld;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class HomeController {
    // khi user truy cập vào endpoint / thì homepage() được gọi
    @GetMapping("/") // dùng để định nghĩa 1 số thành phần trong code
    public String homepage(){
        return "index"; // trả về trang index.html
    }

}
